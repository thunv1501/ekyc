import React, {Component, Fragmen, useEffectt} from 'react';
import {View, StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import store, {reducerNameHOR} from './redux';
import createRootNav, {RoutesName} from './config/routes';

import * as AsyncStore from './utils/AsyncStore';
import {connect} from 'react-redux';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {saveToken} from './redux/tokenAppRDC';
import {setLogin} from './redux/loginRDC';
import {callAPI, AppURL} from './services';
import * as Constants from './config/constants';
import codePush from 'react-native-code-push';
import SplashScreen from 'react-native-splash-screen';
import _ from 'lodash';
import 'react-native-gesture-handler';
console.disableYellowBox = true;
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tokenApp: null,
    };
  }

  async componentDidMount() {
    SplashScreen.hide();
    var clientId = await AsyncStore.getDataFromKey(
      Constants.keyStorage.clientId,
    );
    var tokenApp = await AsyncStore.getDataFromKey(
      Constants.keyStorage.tokenApp,
    );
    this.setState({
      tokenApp: tokenApp,
    });
    store.dispatch(saveToken(JSON.parse(tokenApp)));
    store.dispatch(setLogin(clientId));
  }

  render() {
    let token = this.state.tokenApp;
    let isLogin = token ? true : false;

    var AppNav = createAppContainer(createRootNav('Auth'));
    if (isLogin) {
      AppNav = createAppContainer(createRootNav('App'));
    }
    return (
      <Provider store={store}>
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <StatusBar barStyle="dark-content" />
          <AppNav />
        </View>
      </Provider>
    );
  }
}

export default codePush(App);
