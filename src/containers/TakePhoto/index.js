import React, {Component} from 'react';
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ActivityIndicator,
  StyleSheet,
  Modal,
  Platform,
  Alert,
  SafeAreaView,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import {connect} from 'react-redux';
import {actGetDataAPI} from '../../redux/getDataAPI';
import store, {reducerNameHOR} from '../../redux';
import Dimens from '../../config/Dimens';
import {RNCamera} from 'react-native-camera';
import ImageViewer from 'react-native-image-zoom-viewer';
import Spinner from 'react-native-loading-spinner-overlay';

import * as AsyncStore from '../../utils/AsyncStore';
import {saveToken} from '../../redux/tokenAppRDC';
import * as Constants from '../../config/constants';

import {setLogin} from '../../redux/loginRDC';
import _ from 'lodash';
FontAwesome.loadFont();
const {height, width} = Dimensions.get('window');
const isIOS = Platform.OS == 'ios';
class TakePhoto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      takeImageCurrentNumber: 1,
      photo1: {},
      photo2: {},
      imageViewerData: [],
      isVisibled: false,
      isShowImageViewer: false,
      visible: false,
      focusedScreen: false,
    };
  }

  navigateToProductDetail = () => {
    this.props.navigation.navigate('ProductDetail');
  };

  navigateToSearch = () => {
    this.props.navigation.navigate('Search');
  };

  componentDidMount() {
    const {navigation} = this.props;
    navigation.addListener('willFocus', () =>
      this.setState({focusedScreen: true}),
    );
    navigation.addListener('willBlur', () =>
      this.setState({focusedScreen: false}),
    );
  }

  takePicture = async () => {
    if (this.camera) {
      const options = {quality: 0.2, base64: false};
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
      if (this.state.takeImageCurrentNumber == 1) {
        this.setState({
          photo1: data,
          imageViewerData: [
            {
              url: data.uri,
            },
          ],
          isShowImageViewer: true,
        });
      } else {
        this.setState({
          photo2: data,
          imageViewerData: [
            {
              url: data.uri,
            },
          ],
          isShowImageViewer: true,
        });
      }
    }
  };

  sendToServer = () => {
    var form = new FormData();
    this.setState({
      visible: true,
      isShowImageViewer: false,
      takeImageCurrentNumber: 1,
    });
    if (!_.isEmpty(this.state.photo1)) {
      form.append('frontImg', {
        uri: this.state.photo1.uri,
        type: 'image/jpeg',
        name: this.state.photo1.uri.split('/')[
          this.state.photo1.uri.split('/').length - 1
        ],
      });
    }

    if (!_.isEmpty(this.state.photo2)) {
      form.append('backImg', {
        uri: this.state.photo2.uri,
        type: 'image/jpeg',
        name: this.state.photo2.uri.split('/')[
          this.state.photo2.uri.split('/').length - 1
        ],
      });
    }

    const {token} = store.getState().tokenAppRDC;
    const {clientId} = store.getState().loginReducer;

    this.props
      .actGetDataAPI({
        url: 'ekyc/api/formdata/id',
        name: reducerNameHOR.updateProfile,
        params: form,
        method: 'form',
        token: token,
        clientId: clientId,
      })
      .then(data => {
        this.setState({
          visible: false,
        });
        if (data.code == 200) {
          let paramsData = data.data;
          paramsData.imageFront = this.state.photo1.uri;
          paramsData.imageBack = this.state.photo2.uri;
          this.props.navigation.navigate('RecordVideo', {
            data: paramsData,
          });
        } else if (data.code == 401) {
          setTimeout(() => {
            Alert.alert('Thông báo', data.message);
          }, 300);
          AsyncStore.removeDataFromKey(Constants.keyStorage.tokenApp);
          AsyncStore.removeDataFromKey(Constants.keyStorage.clientId);
          store.dispatch(setLogin(''));
          store.dispatch(saveToken(''));
          this.props.navigation.push('Auth');
        } else {
          setTimeout(() => {
            Alert.alert('Thông báo', data.message);
          }, 300);
        }
      })
      .catch(err => {
        console.log('-errrr');
        Alert.alert('Thông báo', err);
      });
  };

  renderFooter = () => {
    return (
      <View
        style={{
          width: '100%',
          height: 60,
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: Dimens.Margin.Normal,
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity
          style={{
            paddingVertical: Dimens.Margin.Normal,
            paddingHorizontal: Dimens.Margin.Normal,
          }}
          onPress={() => {
            if (this.state.takeImageCurrentNumber == 1) {
              this.setState({
                isShowImageViewer: false,
                takeImageCurrentNumber: 2,
              });
            } else {
              this.sendToServer();
            }
          }}>
          <Text style={{color: 'white', fontSize: 15}}>Tiếp tục</Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderHeader = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingHorizontal: Dimens.Margin.Normal,
          width: '100%',
          height: 60,
        }}>
        <Text style={{color: 'white', fontSize: 16}}>Huỷ bỏ</Text>
        <View style={{flex: 1}}>
          <Text style={{color: 'white', fontSize: 18, textAlign: 'center'}}>
            Anh mat truoc
          </Text>
        </View>
        <Text />
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.state.focusedScreen ? (
          <View style={{flex: 1}}>
            <Spinner visible={this.state.visible} />
            <View style={{flex: 1}}>
              <View style={styles.container}>
                <RNCamera
                  ref={ref => {
                    this.camera = ref;
                  }}
                  style={styles.preview}
                  type={RNCamera.Constants.Type.back}
                  androidCameraPermissionOptions={{
                    title: 'Permission to use camera',
                    message: 'We need your permission to use your camera',
                    buttonPositive: 'Ok',
                    buttonNegative: 'Cancel',
                  }}
                  androidRecordAudioPermissionOptions={{
                    title: 'Permission to use audio recording',
                    message: 'We need your permission to use your audio',
                    buttonPositive: 'Ok',
                    buttonNegative: 'Cancel',
                  }}>
                  <View
                    style={{
                      width: width - 40,
                      height: (width - 40) / 1.5,
                      borderWidth: 5,
                      borderColor: 'green',
                    }}
                  />
                </RNCamera>
                <View
                  style={{
                    width: '100%',
                    height: 60,
                    backgroundColor: 'rgba(52, 52, 52, 0.4)',
                    alignItems: 'center',
                    position: 'absolute',
                    top: 0,
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 16,
                      textAlign: 'center',
                    }}>
                    {this.state.takeImageCurrentNumber == 1
                      ? 'Chụp ảnh mặt trước CMT'
                      : 'Chụp ảnh mặt sau CMT'}
                  </Text>
                </View>

                <View
                  style={{
                    flex: 0,
                    flexDirection: 'row',
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => this.takePicture()}
                    style={styles.capture}>
                    <Text style={{fontSize: 14}}>Chụp ảnh</Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    flex: 0,
                    flexDirection: 'row',
                    justifyContent: 'center',
                  }}
                />
              </View>
              {this.state.imageViewerData && (
                <Modal
                  onRequestClose={() =>
                    this.setState({isShowImageViewer: false})
                  }
                  visible={this.state.isShowImageViewer}
                  transparent={true}>
                  <ImageViewer
                    imageUrls={this.state.imageViewerData}
                    enableSwipeDown={true}
                    index={0}
                    onClick={() => this.setState({isShowImageViewer: false})}
                    enablePreload={true}
                    onSwipeDown={() =>
                      this.setState({isShowImageViewer: false})
                    }
                    swipeDownThreshold={100}
                    renderFooter={this.renderFooter}
                    menus={() => {}}
                    menuContext={{
                      saveToLocal: 'save to the album',
                      cancel: 'cancel',
                    }}
                  />
                </Modal>
              )}
            </View>
          </View>
        ) : (
          <View />
        )}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actGetDataAPI: action => dispatch(actGetDataAPI(action)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TakePhoto);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});
