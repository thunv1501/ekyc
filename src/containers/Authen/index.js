import React, {Component} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  TextInput,
  Dimensions,
  Alert,
} from 'react-native';
import {Button} from 'react-native-elements';
import _ from 'lodash';

import {connect} from 'react-redux';
import {actGetDataAPI} from '../../redux/getDataAPI';
import store, {reducerNameHOR} from '../../redux';
import * as AsyncStore from '../../utils/AsyncStore';
import {saveToken} from '../../redux/tokenAppRDC';
import * as Constants from '../../config/constants';
import {setLogin} from '../../redux/loginRDC';

class Authen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clientId: 'client_demo',
      secretKey: 'SVRzQDIwMTk=',
      isLoading: false,
    };
  }

  login = () => {
    if (_.isEmpty(this.state.clientId) && _.isEmpty(this.state.secretKey)) {
      Alert.alert('Thông báo', 'Vui lòng nhập Client Id và Secret key');
    } else if (_.isEmpty(this.state.clientId)) {
      Alert.alert('Thông báo', 'Vui lòng nhập Client Id');
    } else if (_.isEmpty(this.state.secretKey)) {
      Alert.alert('Thông báo', 'Vui lòng nhập Secret key');
    } else {
      this.setState({
        isLoading: true,
      });
      this.props
        .actGetDataAPI({
          url: 'ekyc/api/auth/login',
          name: reducerNameHOR.login,
          params: JSON.stringify({
            client_id: this.state.clientId.trim(),
            secret_key: this.state.secretKey.trim(),
          }),
          method: 'post',
        })
        .then(data => {
          this.setState({
            isLoading: false,
          });
          if (data.code !== 200) {
            Alert.alert('Thông báo', 'Thông tin đăng nhập không chính xác');
          } else {
            let token = data.data;
            AsyncStore.saveDataFromKey(
              Constants.keyStorage.tokenApp,
              JSON.stringify(token),
            );
            AsyncStore.saveDataFromKey(
              Constants.keyStorage.clientId,
              this.state.clientId.trim(),
            );
            store.dispatch(saveToken(token));
            store.dispatch(setLogin(this.state.clientId.trim()));
            this.props.navigation.replace('App');
          }
          console.log('-----', data);
        })
        .catch(err => {
          Alert.alert('Thông báo', err);
          this.setState({
            isLoading: false,
          });
        });
    }
  };
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={{flex: 1, paddingHorizontal: 20}}>
          <Text
            style={{
              textAlign: 'center',
              marginTop: 50,
              fontSize: 30,
              fontWeight: '600',
              color: '#3b83b3',
            }}>
            ITS GW
          </Text>
          <Text style={{fontSize: 24, fontWeight: '600', marginTop: 70}}>
            Đăng nhập để tiếp tục
          </Text>
          <View style={{marginTop: 30}}>
            <View>
              <Text style={{color: '#333333', fontSize: 14}}>Client ID</Text>
              <TextInput
                value={this.state.clientId}
                onChangeText={value => this.setState({clientId: value})}
                style={{
                  width: '100%',
                  height: 40,
                  borderWidth: 1,
                  borderColor: '#cccccc',
                  borderRadius: 5,
                  marginTop: 5,
                  fontSize: 14,
                  paddingHorizontal: 10,
                }}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text style={{color: '#333333', fontSize: 14}}>Secret key</Text>
              <TextInput
                value={this.state.secretKey}
                onChangeText={value => this.setState({secretKey: value})}
                style={{
                  width: '100%',
                  height: 40,
                  borderWidth: 1,
                  borderColor: '#cccccc',
                  borderRadius: 5,
                  marginTop: 5,
                  fontSize: 14,
                  paddingHorizontal: 10,
                }}
              />
            </View>
          </View>
          <Button
            title={'Đăng nhập'}
            onPress={this.login}
            loading={this.state.isLoading}
            disabled={this.state.isLoading}
            titleStyle={{fontSize: 14, fontWeight: 'bold'}}
            buttonStyle={{
              width: '100%',
              height: 40,
              backgroundColor: '#3b83b3',
              marginTop: 30,
            }}
          />
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actGetDataAPI: action => dispatch(actGetDataAPI(action)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Authen);
