import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Safe,
  SafeAreaView,
  ScrollView,
  Dimensions,
  Image,
} from 'react-native';
import Header from '../../components/Header';

import {connect} from 'react-redux';
import {actGetDataAPI} from '../../redux/getDataAPI';
import Dimens from '../../config/Dimens';

const {width, height} = Dimensions.get('window');

class Result extends Component {
  render() {
    let info = this.props.updateProfile.data.data;
    let data = this.props.navigation.state.params.data;
    return (
      <SafeAreaView style={{flex: 1}}>
        <Header
          leftAction={() => this.props.navigation.popToTop()}
          title="Kết quả xác thực khuôn mặt"
        />
        <View style={{flex: 1}}>
          <ScrollView style={{flex: 1}}>
            <View
              style={{
                width: '100%',
                alignItems: 'center',
                paddingVertical: 2 * Dimens.Margin.Normal,
              }}>
              <Image
                source={
                  data.face
                    ? {uri: data.face}
                    : require('../../assets/images/icons/avatar.jpg')
                }
                style={{width: 100, height: 100, borderRadius: 50}}
              />
            </View>

            <View>
              <Text
                style={{
                  marginTop: 30,
                  fontSize: 24,
                  fontWeight: 'bold',
                  marginHorizontal: Dimens.Margin.Normal,
                  marginBottom: 20,
                }}>
                Thông tin cá nhân
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>Loại giấy tờ</Text>
              </View>
              <View style={{flex: 1}}>
                <Text>{info && info.type ? info.type : null}</Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>Số</Text>
              </View>
              <View style={{flex: 1}}>
                <Text>{info && info.id_number ? info.id_number : null}</Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>Họ và tên</Text>
              </View>
              <View style={{flex: 1}}>
                <Text>{info && info.name ? info.name : null}</Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>Ngày sinh</Text>
              </View>
              <View style={{flex: 1}}>
                <Text>{info && info.dob ? info.dob : null}</Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>Giới tính</Text>
              </View>
              <View style={{flex: 1}}>
                <Text>{info && info.gender ? info.gender : null}</Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>Nguyên quán</Text>
              </View>
              <View style={{flex: 1}}>
                <Text>{info && info.home ? info.home : null}</Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>
                  Hộ khẩu thường trú
                </Text>
              </View>
              <View style={{flex: 1}}>
                <Text>{info && info.residence ? info.residence : null}</Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>Nơi cấp</Text>
              </View>
              <View style={{flex: 1}}>
                <Text>{info && info.poi ? info.poi : null}</Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>Ngày cấp</Text>
              </View>
              <View style={{flex: 1}}>
                <Text>{info && info.doi ? info.doi : null}</Text>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>Ngày hết hạn</Text>
              </View>
              <View style={{flex: 1}}>
                <Text>{info && info.doe ? info.doe : null}</Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>Tôn giáo</Text>
              </View>
              <View style={{flex: 1}}>
                <Text>{info && info.religion ? info.religion : null}</Text>
              </View>
            </View>
            <View>
              <Text
                style={{
                  marginTop: 30,
                  fontSize: 24,
                  fontWeight: 'bold',
                  marginHorizontal: Dimens.Margin.Normal,
                }}>
                Kết quả khớp khuôn mặt
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>Kết quả</Text>
              </View>
              <View style={{flex: 1}}>
                <Text>
                  {data.is_matched ? 'Giống nhau' : 'Không giống nhau'}
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                alignItems: 'center',
                borderColor: '#eeeeee',
                paddingHorizontal: Dimens.Margin.Normal,
                paddingVertical: 15,
              }}>
              <View style={{width: 120}}>
                <Text style={{fontSize: 13, color: 'gray'}}>Độ tương đồng</Text>
              </View>
              <View style={{flex: 1}}>
                <Text>{data.matching_score}%</Text>
              </View>
            </View>

            <View
              style={{
                marginTop: Dimens.Margin.Normal,
                paddingHorizontal: Dimens.Margin.Normal,
                borderBottomWidth: 1,
                borderColor: '#eeeeee',
              }}>
              <Text style={{fontSize: 13, color: 'gray'}}>
                Ảnh chụp mặt trước
              </Text>
              <View style={{width: '100%', alignItems: 'center'}}>
                <Image
                  resizeMode="contain"
                  source={{uri: data.imageFront}}
                  style={{width: width / 2, height: width}}
                />
              </View>
            </View>
            <View
              style={{
                marginTop: Dimens.Margin.Normal,
                paddingHorizontal: Dimens.Margin.Normal,
              }}>
              <Text>Ảnh chụp mặt sau</Text>
              <View style={{width: '100%', alignItems: 'center'}}>
                <Image
                  resizeMode="contain"
                  source={{uri: data.imageBack}}
                  style={{width: width / 2, height: width}}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    // actGetDataAPI: action => dispatch(actGetDataAPI(action)),
    actGetDataAPI: action => dispatch(actGetDataAPI(action)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Result);
