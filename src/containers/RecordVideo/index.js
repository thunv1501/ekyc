import React, {Component} from 'react';
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ActivityIndicator,
  InteractionManager,
  StyleSheet,
  Alert,
  Image,
  SafeAreaView,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import {connect} from 'react-redux';
import {actGetDataAPI} from '../../redux/getDataAPI';
import store, {reducerNameHOR} from '../../redux';
import {RNCamera} from 'react-native-camera';
import {captureScreen, captureRef} from 'react-native-view-shot';
import CountDown from '../../components/CountDown';
import {withNavigationFocus} from 'react-navigation';
import * as AsyncStore from '../../utils/AsyncStore';
import {saveToken} from '../../redux/tokenAppRDC';
import * as Constants from '../../config/constants';
import MarqueeText from 'react-native-marquee';

import {setLogin} from '../../redux/loginRDC';
import Dimens from '../../config/Dimens';

FontAwesome.loadFont();
const {height, width} = Dimensions.get('window');
const itemWidth = (width - 30) / 2;
class RecordVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      takeImageCurrentNumber: 1,
      image1Base64: '',
      image2Base64: '',
      loading: true,
      time: 0,
      recorded: false,
      recordedData: null,
      isRecording: false,
      isShowVideo: false,
      faceIndex: 1,
      videoUri: '',
      cameraType: 'back',
    };
  }

  stopCapture = () => {
    this.camera.stopRecording();
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        cameraType: 'front',
      });
    }, 1000);
  }

  renderTimer() {
    const {isRecording, time, recorded} = this.state;
    return (
      <View style={{position: 'absolute', backgroundColor: 'black', top: 0}}>
        {(recorded || isRecording) && (
          <Text>
            <Text style={{color: 'white'}}>●</Text>{' '}
            {this.convertTimeString(time)}
          </Text>
        )}
      </View>
    );
  }

  async startRecording() {
    this.setState({isRecording: true});
    // default to mp4 for android as codec is not set
    this.camera
      .recordAsync()
      .then(data => {
        console.log('video capture', data);
        this.setState({isRecording: false, processing: true});
        this.sendData(data.uri);
      })
      .catch(err => console.error(err));
  }

  sendData = uri => {
    let sessionId = this.props.navigation.state.params.data.session;
    let imageFront = this.props.navigation.state.params.data.imageFront;
    let imageBack = this.props.navigation.state.params.data.imageBack;
    var form = new FormData();

    form.append('video', {
      name: 'mobile-video-upload.mp4',
      type: 'video/mp4',
      uri: uri,
    });

    const {token} = store.getState().tokenAppRDC;
    const {clientId} = store.getState().loginReducer;

    this.props
      .actGetDataAPI({
        url: `ekyc/api/formdata/video?msgId=111&sessionId=${sessionId}`,
        name: reducerNameHOR.getWard,
        params: form,
        method: 'form',
        token: token,
        clientId: clientId,
      })
      .then(data => {
        console.log('-----', data);
        this.setState({
          isRecording: false,
          processing: false,
        });
        if (data.code == 200) {
          let paramData = data.data;
          paramData.imageFront = imageFront;
          paramData.imageBack = imageBack;
          this.props.navigation.navigate('Result', {data: paramData});
        } else if (data.code == 401) {
          Alert.alert('Thông báo', data.message);
          AsyncStore.removeDataFromKey(Constants.keyStorage.tokenApp);
          AsyncStore.removeDataFromKey(Constants.keyStorage.clientId);
          store.dispatch(setLogin(''));
          store.dispatch(saveToken(''));
          this.props.navigation.replace('Auth');
        } else {
          this.setState({
            isRecording: false,
            processing: false,
          });
          Alert.alert(
            'Thông báo',
            data.message ? data.message : 'Có lỗi xảy ra',
          );
        }
      })
      .catch(err => {
        Alert.alert('Thông báo', err.message);
      });
  };

  stopRecording() {
    this.camera.stopRecording();
  }

  render() {
    let {isRecording, processing} = this.state;
    let info = this.props.updateProfile.data.data;
    let button = (
      <TouchableOpacity
        onPress={this.startRecording.bind(this)}
        style={styles.capture}>
        <Text style={{fontSize: 14}}>Bắt đầu</Text>
      </TouchableOpacity>
    );

    if (isRecording) {
      button = (
        <TouchableOpacity style={styles.capture}>
          <CountDown
            size={20}
            until={5}
            onFinish={() => this.stopCapture()}
            timeLabelStyle={{color: 'red', fontWeight: 'bold'}}
            separatorStyle={{color: 'black', fontSize: 16}}
            timeToShow={['M', 'S']}
            timeLabels={{m: null, s: null}}
            digitStyle={{width: 30, height: 30, backgroundColor: 'white'}}
            digitTxtStyle={{
              color: 'red',
              fontSize: 16,
              fontWeight: 'bold',
            }}
            showSeparator
          />
        </TouchableOpacity>
      );
    }

    if (processing) {
      button = (
        <View style={styles.capture}>
          <ActivityIndicator animating size={18} />
        </View>
      );
    }

    let isActive = this.props.navigation.isFocused();
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.container}>
          <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style={styles.preview}
            type={this.state.cameraType}
            flashMode={RNCamera.Constants.FlashMode.off}
            captureAudio={false}
            mute
            defaultVideoQuality={RNCamera.Constants.VideoQuality['720p']}
            playSoundOnCapture={false}
            permissionDialogTitle={'Permission to use camera'}
            permissionDialogMessage={
              'We need your permission to use your camera phone'
            }>
            <View
              style={{
                width: width - 40,
                height: width - 40,
                borderWidth: 5,
                borderColor: 'green',
              }}
            />
          </RNCamera>
          <View
            style={{
              width: '100%',
              height: 60,
              backgroundColor: 'rgba(52, 52, 52, 0.4)',
              alignItems: 'center',
              position: 'absolute',
              top: 0,
              justifyContent: 'center',
              paddingHorizontal: Dimens.Margin.Normal,
            }}>
            <Text style={{color: 'white', fontSize: 16, textAlign: 'center'}}>
              Bạn hãy quay mặt của bạn và đọc câu sau
            </Text>
            <MarqueeText
              style={{fontSize: 16, color: 'green'}}
              duration={3000}
              marqueeOnStart
              loop
              marqueeDelay={1000}
              marqueeResetDelay={1000}>
              {info && info.subtitle ? info.subtitle : null}
            </MarqueeText>
          </View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            {button}
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    // actGetDataAPI: action => dispatch(actGetDataAPI(action)),
    actGetDataAPI: action => dispatch(actGetDataAPI(action)),
  };
}

export default withNavigationFocus(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(RecordVideo),
);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  backgroundVideo: {
    flex: 1,
  },
});
