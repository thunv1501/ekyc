import { put, takeEvery, call, all, fork } from "redux-saga/effects";
import getDataAPISaga from "./getDataAPI";

function* rootSaga() {
  yield all([fork(getDataAPISaga)]);
}

export default rootSaga;
