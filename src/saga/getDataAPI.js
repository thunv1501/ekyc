import {
  GET_DATA_API,
  GET_DATA_API_SUCCESS,
  GET_DATA_API_FAIL,
  actGetDataAPI,
} from '../redux/getDataAPI';
// import * as ToastRDC from "../Redux/toastRDC";
import {put, takeEvery, call, all, fork} from 'redux-saga/effects';
import {delay} from 'redux-saga';
import {callAPI, URL, AppURL} from '../services';
import Lodash from 'lodash';
import store from '../redux';
import * as Utils from '../utils';
import * as AsyncStore from '../utils/AsyncStore';
import * as Constants from '../config/constants';
import {saveToken, TOKEN_APP} from '../redux/tokenAppRDC';
import {encodeToken, decodeToken} from '../utils/TokenManager';

import moment from 'moment';

function* handelError(action, response) {
  yield put({
    type: GET_DATA_API_FAIL,
    name: action.name,
  });

  action.reject(null);

  yield call(delay, 100);
}

function* refreshToken(action, res, err) {
  const {token} = store.getState().tokenAppRDC;
  var params = {
    token: token,
  };
  const {response, errMessage} = yield callAPI(
    AppURL.refreshToken,
    'post',
    params,
  );
  if (response) {
    if (
      Lodash.isEqual(response.status, 200) ||
      Lodash.isEqual(response.status, 201)
    ) {
      var newToken = response.data.token;
      console.log('---------', response);
      AsyncStore.saveDataFromKey(
        Constants.keyStorage.tokenApp,
        JSON.stringify(newToken),
      );
      yield put({
        type: TOKEN_APP,
        token: newToken,
      });
      let newAction = {
        ...action,
        params: {...action.params},
        token: newToken.access_token,
      };
      yield put({
        type: GET_DATA_API,
        url: newAction.url,
        notLoading: newAction.notLoading,
        notToast: newAction.notToast,
        name: newAction.name,
        params: newAction.params,
        token: newAction.token,
        method: action.method,
        resolve: newAction.resolve,
        reject: newAction.reject,
      });
    } else if ((Lodash.isEqual(response.errorCode), 401)) {
      yield* refreshToken(action, response, errMessage);
    } else {
      yield* handelError(action, response, errMessage);
    }
  } else {
    yield* handelError(action, response, errMessage);
  }
}

function* getDataAPI(action) {
  const {response, errMessage} = yield callAPI(
    action.url,
    action.method,
    action.params,
    action.token,
    action.clientId,
  );
  // console.log("RES SAGA", response);
  if (response) {
    if (
      Lodash.isEqual(response.status, 200) ||
      Lodash.isEqual(response.status, 201)
    ) {
      yield put({
        type: GET_DATA_API_SUCCESS,
        data: response.data,
        name: action.name,
      });

      action.resolve(response.data);
    } else if (Lodash.isEqual(response.status, 403)) {
      yield* refreshToken(action, response, errMessage);
    } else {
      action.reject(response.data);
    }
  }
}

function* getDataAPISaga() {
  yield takeEvery(GET_DATA_API, getDataAPI);
}

export default getDataAPISaga;
