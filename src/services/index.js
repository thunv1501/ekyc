import APISauce from 'apisauce';
import {errorMessage} from '../config/constants';
import Lodash from 'lodash';
import {Platform} from 'react-native';
import _ from 'lodash';
const TIMEOUT_ERROR = 'TIMEOUT_ERROR';
const NETWORK_ERROR = 'NETWORK_ERROR';

var baseURL = 'http://123.31.17.59:8079'; //product

export const AppURL = {
  base: baseURL,
};

export const apiGlobal = token => {
  return APISauce.create({
    baseURL: AppURL.base,
    timeout: 30000,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
};

export const callAPI = (tailURL, method, params, token, clientId) => {
  if (method == 'get') {
    return APISauce.create({
      baseURL: AppURL.base,
      timeout: 30000,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .get(tailURL)
      .then(response => {
        console.log('------respo', response);
        return {response: response};
      });
  } else if (method == 'post') {
    return APISauce.create({
      baseURL: AppURL.base,
      timeout: 30000,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .post(tailURL, params)
      .then(response => {
        console.log('------respo', response);
        return {response: response};
      });
  } else if (method == 'put') {
    return APISauce.create({
      baseURL: AppURL.base,
      timeout: 30000,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .put(tailURL, params)
      .then(response => {
        console.log('------respo', response);
        return {response: response};
      });
  } else if (method == 'delete') {
    return APISauce.create({
      baseURL: AppURL.base,
      timeout: 30000,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .delete(tailURL)
      .then(response => {
        return {response: response};
      });
  } else if (method == 'form') {
    return APISauce.create({
      baseURL: AppURL.base,
      timeout: 30000,
      headers: {
        'Content-Type': 'multipart/form-data',
        'x-access-token': token,
        client_id: clientId,
      },
    })
      .post(tailURL, params)
      .then(response => {
        return {response: response};
      });
  }
};
