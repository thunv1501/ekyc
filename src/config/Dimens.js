const baseMargin = 10;
const baseViewHeight = 40;
const baseFontSize = 13;

export default {
  Margin: {
    Extra_Short: 0.25 * baseMargin,
    Short: 0.5 * baseMargin,
    Normal: baseMargin,
    Long: 1.5 * baseMargin,
    Extra_Long: 2 * baseMargin,
    defaultMargin: baseMargin,
  },
  FontSize: {
    Small: 0.75 * baseFontSize,
    Normal: baseFontSize,
    Big: 1.25 * baseFontSize,
    VeryBig: 2 * baseFontSize,
  },
};
