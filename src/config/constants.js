export const GOOGLE_MAP_KEY = 'AIzaSyCa32FxcLM-p6kaeVl8pqF0P6hYLNNPMbU';
//old key: //"AIzaSyCCpQIC8DS3okdC72FPAq6-Gls4JmdGZUA";

export const BUGSNAG_KEY = 'c33312de9dc108134075c4b04a888e3c';
export const FIREBASE_KEY = 'AIzaSyDEVS7Eg04jmSweFuV4BIaI9KP0xKD3HSQ';
export const RANGE_DIFF = 3000;
export const PHONE_SUPPORT = '0329668048';
export const LALA_PHONE_SUPPORT = '1900636814';
export const ONESIGNAL_KEY = 'bea3f412-6549-4e5a-9415-28427e9eaa9d';
export const PUBLIC_KEY_DECODE = `-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKJ0DG8rAGeK0qfvn11vRR9SjCh2zexC
IN2IzdViFv2TuURyCEC8Dv/0B+Sp0qspdoj4DsgSavSQnYEn8lLaq6cCAwEAAQ==
-----END PUBLIC KEY-----`;
export const PRIVATE_KEY_ENDCODE = `-----BEGIN RSA PRIVATE KEY-----
MIIBOgIBAAJBAKJ0DG8rAGeK0qfvn11vRR9SjCh2zexCIN2IzdViFv2TuURyCEC8
Dv/0B+Sp0qspdoj4DsgSavSQnYEn8lLaq6cCAwEAAQJAWjnX+hpJBDj/HNSij3NQ
UcA4JfUuE6z6se/cWko7eKQgxqHo0nhDDfDO2eAh3qyUROkogvb0ZnpsT5LN0hja
eQIhAO+LXQP7JD80h8+j1qLMap+3KLfGgefNkWG1Kw2v2SZ1AiEArZz1bXUGKIYH
CssIoHG4pn3EhuTfemLO6QQ8/2JxnisCICiiPlQHjP1/vq31P9p7zHZtTmXmMXEN
yOSHI9d9hdtlAiAzNvs7l+sCJ0+Kgox/8Cw9iRphhMDcNKu/F4369Ip50wIhAMuF
ohRpeTLr9DPKhUZ5TdDrxZrXklJiiAku5MC5iSM4
-----END RSA PRIVATE KEY-----`;
export const COIN_TYPE = 2;
export const GIFT_TYPE = 1;
export const CHECK_APP_INSTALL_LIST = [
  ['vn.tiki.app.Tiki', 'vn.tiki.app.tikiandroid'],
  ['cinema.cgv.vn', 'com.cgv.cinema.vn'],
  ['sendo', 'com.sendo'],
  ['deliverynow', 'com.deliverynow'],
  ['nowmerchant', 'com.sea.foody.nowmerchant'],
  ['foody', 'com.foody.vn.activity'],
  ['tch', 'com.thecoffeehouse.guestapp'],
  ['starbuckshk', 'com.starbucks.mobilecard'],
  ['vietnammm', 'com.vietnammm.android'],
  ['shopiness', 'com.nct.shopiness'],
  ['meete', 'com.cixa.meete'],
  ['meetevn', ''],
  ['lixiapp', 'com.opencheck.client'],
  ['lixi', ''],
  ['jamjalinks', 'com.jamjavn.jamja'],
  ['ifind', 'com.arcasolutions.ifindFi'],
  ['momo', 'com.mservice.momotransfer'],
  ['airpaysdk', 'com.beeasy.airpay'],
  ['airpaysdkvn', ''],
  ['zalopay', 'vn.com.vng.zalopay'],
  ['uber', 'com.ubercab'],
  ['grab', 'com.grabtaxi.passenger'],
  ['goviet', 'com.goviet.app'],
  ['instagram', 'com.instagram.android'],
  ['whatsapp', 'com.whatsapp'],
  ['line', ' jp.naver.line.android'],
  ['zalo', 'com.zing.zalo'],
  ['shopeevn', 'com.shopee.vn'],
  ['LAZADA', 'com.lazada.android'],
];

export const DEFAULT_LIST = [
  0,
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11,
  12,
  13,
  14,
  15,
  16,
  17,
  18,
  19,
  20,
];

export const statusNotifiOrder = {
  cancelStt: 'CANCELED',
  successStt: 'COMPLETED',
  inprogressStt: 'IN_PROCESS',
  accepted: 'ACCEPTED',
  assignStt: 'status_assigning',
};

export const keyStorage = {
  tokenLogin: 'app/TokenLogin',
  tokenApp: 'app/TokenApp',
  locationUser: 'app/LocationUser',
  locationUserChosen: 'app/LocationUserChosen',
  isChosenLocation: 'app/IsChosenLocation',
  isFistTimeOpenApp: 'app/isFistTimeOpenApp',
  nameCity: 'home/nameCity',
  historyLocation: 'home/historyLocation',
  idCheckIn: 'home/idCheckIn',
  checkAppInstall: 'checkAppInstall',
  cartUser: 'cartUser',
  timeCreateCart: 'timeCreateCart',
  orderCancle: 'orderCancle',
  oneSignalId: 'oneSignalId',
  isLogin: 'isLogin',
  clientId: 'clientId',
};

export const errorMessage = {
  timeOutRequest: 'VCED không phản hồi!',
  networkError: 'Không thể kết nối tới VCED!',
  sysError: 'VCED tạm thời gặp lỗi! Vui lòng thử lại sau!',
};

export const typeLogin = {
  mobile: 'mobile',
  google: 'google',
  facebook: 'facebook',
};

export const fireStoreKey = {
  ListStore: 'ListStore',
  ListComment: 'ListComment',
  ListImageComment: 'ListImageComment',
};

export const keyParamsNav = {
  typeLogin: 'typeLogin',
  phoneRegister: 'phoneRegister',
  gmailRegister: 'gmailRegister',
  nameRegister: 'nameRegister',
  typeAffiliate: 'typeAffiliate',
  idProduct: 'idProduct',
  //Home.navState
  itemStore: 'itemStore',
  infoGetDetailStore: 'infoGetDetailStore',
  isFromTabNav: 'isFromTabNav',
  reloadHome: 'reloadHome',
  paramsSearchStore: 'paramsSearchStore',
  paramsSearchStoreTag: 'paramsSearchStoreTag',
  paramsShopingCart: 'paramsShopingCart',
  idCheckIn: 'idCheckIn',
  itemGiftCheckIn: 'itemGiftCheckIn',
  indexGiftCheckIn: 'indexGiftCheckIn',

  //User
  store: 'store',
  nameUser: 'nameUser',
  callBackUpdateProfile: 'callBackUpdateProfile',
  levelUser: 'levelUser',
  referCode: 'referCode',
  birthday: 'birthday',
  userInfo: 'userInfo',
  loginSuccess: 'loginSuccess',

  // Gift
  gift: 'gift',
  detailExchange: 'detailExchange',
  infoGiftExchange: 'infoGiftExchange',
  fromRoute: 'fromRoute',
  coinUser: 'coinUser',
  exchangeGiftSuccess: 'exchangeGiftSuccess',

  //Coin
  promotion: 'promotion',

  //store detail 2.
  chooseTopping: 'chooseTopping',
  nameTopping: 'nameTopping',

  //created order.
  createdOrderInfo: 'createdOrderInfo',
  distanceKm: 'distanceKm',
  storeNote: 'storeNote',
  prevScreen: 'previousScreen',
  typeOrder: 'typeOrder',

  // key dynamic link navigation:
  goToDetailStore: '1',
  goToDetailPromotion: '0',

  idStoreV2: 'idStoreV2',
  nameStore: 'nameStore',
  idVoucher: 'idVoucher',
  itemVoucher: 'itemVoucher',
  itemListStoreCate: 'itemListStoreCate',
  idListStoreCate: 'idListStoreCate',
  idPartner: 'idPartner',
  discountOrderCb: 'discountOrderCb',
  idGift: 'idGift',
  linkPartner: 'linkPartner',
  discountPercent: 'discountPercent',
  nameStoreLink: 'nameStoreLink',
  idCampaign: 'idCampaign',
  idItem: 'idItem',

  fbId: 'fbId',
  ggId: 'ggId',
  mobile: 'mobile',
};

export const keyDeepLink = {
  goToDetailStore: '1',
  goToDetailEvoucher: '2',
  goToOrderStore: '3',
  goToListCate: '4',
  goToDetailGift: '5',
  goToCart: '6',
  keyIdItem: 'id',
  type: 'type',
  goToStore: 'store',
  goToStores: 'stores',
  goToGift: 'gift',
  goToGifts: 'gifts',
  goToDiscover: 'discover',
  goToCheckIn: 'checkin',
  goToPolicy: 'policy',
  goToCoin: 'earncoin',
  goToNotification: 'notifications',
  goToOrderHistory: 'orderhistory',
  goToCoinHistory: 'coinhistory',
  goToReferFriend: 'referfriend',
  goToReferHistory: 'referhistory',
  goToCateList: 'hot',
  goToCurrentCart: 'checkout',
  typeAff: 'aff',
  goToCampaignDetail: 'campaigns',
  typeVerify: 'verify',
  typeOther: 'other',
  affiliate: 'affiliate',
  idItem: 'idItem',
};

export const keyEventAnalytic = {
  // dynamic link
  utmSource: 'utm_source',
  keyIdItem: 'id',
  keyIdPromo: 'id_promo',
  keyCatPromo: 'cat_promo',
  keyGoToPage: 'go_to_page',

  swipeIntroScreen: 'swipe_introduce_screen',
  clickIntroBtn: 'click_intro_button',
  sourceInstall: 'source_install',
  clickSearchStore: 'click_search_store',
  clickStoreHome: 'click_store_home',
  clickLoadMoreHome: 'click_loadmore_home',
  clickBannerHome: 'click_banner_home',
  clickLoginHome: 'click_login_home',

  // store search.
  searchWithText: 'search_with_text',

  //store detail
  orderStoreDetail: 'order_store_detail',
  viewStore: 'view_store',

  // cart.
  addToCart: 'add_to_cart',
  checkOutCart: 'check_out_cart',
  createOrderCart: 'create_order_cart',

  // promotion.
  clickPromotion: 'click_promotion',

  // list gift.
  clickGift: 'click_gift',
  buyGift: 'buy_gift',

  // webview
  userOrderFlow: 'user_order_flow',
  orderViewStore: 'order_view_store',
  orderCheckOut: 'order_checkout',
  orderCreatedBill: 'order_created_bill',
  keyViewStore: 'giao-do-an',
  keyCheckOut: 'checkout',
  keyCreatedBill: 'order_id',

  // click go to setting, LocationPermission.
  clickGoSetting: 'click_go_setting',
  askPermissionAndroid: 'ask_permission_android',

  // login social screen.
  clickLoginFb: 'click_login_fb',
  clickLoginGG: 'click_login_gg',
  clickLoginPhone: 'click_login_phone',

  // register.
  clickRegister: 'click_register',

  // New event facebook.
  completeRegister: 'fb_mobile_complete_registration',
  mobileSearchClick: 'fb_mobile_search',
  keySearchString: 'fb_search_string',
  mobileViewContent: 'fb_mobile_content_view',
  mobileViewContentOrder: 'fb_mobile_content_view_order',
  keyContentId: 'fb_content_id',
  keyProductId: 'fb_product_item_id',
  keyContent: 'fb_content',
  keyProductTitle: 'fb_product_title',
  keyContentType: 'fb_content_type',
  mobileAddToCart: 'fb_mobile_add_to_cart',
  keyDescription: 'fb_description',
  keyProductCate: 'fb_product_category',
  listProductCart: 'list_product_cart',
  keyPriceAmount: 'fb_product_price_amount',
  keyCurrency: 'fb_currency',
  mobileCheckOut: 'fb_mobile_initiated_checkout',
  mobileClickCashBack: 'fb_mobile_click_cashback',
  keyNumItems: 'fb_num_items',
  keyPaymentMethod: 'fb_payment_info_available',
  mobilePurchase: ' fb_mobile_purchase',
  mobileViewEvoucher: 'fb_mobile_content_view_evoucher',
  keyNameItem: 'fb_level',
  numItems: 'fb_num_items',
  productCurrency: 'fb_product_price_currency',
  sumValue: 'valueToSum',
  keyCoupon: 'fb_coupon_used',

  // new event google.
  signUpGG: 'sign_up',
  searchGG: 'search',
  keySearchTermGG: 'search_term',
  viewStoreGG: 'view_item',
  keyItemIdGG: 'item_id',
  keyItemNameGG: 'item_name',
  keyItemCateGG: 'item_category',
  addToCartGG: 'add_to_cart',
  keyCurrencyGG: 'currency',
  keyPriceGG: 'price',
  keyValueGG: 'value',
  checkOutCartGG: 'begin_checkout',
  purchaseGG: 'ecommerce_purchase',
  keyCouponGG: 'coupon',
  keyDestinationGG: 'destination',

  // Check in program.
  shareFacebook: 'share_facebook',
};

export const keyPermission = {
  location: 'location',
  notification: 'notification',
  authorized: 'authorized',
  granted: 'granted',
  hadPermission: '1',
  notHadPermission: '0',
};

export const appState = {
  inactive: 'inactive',
  background: 'background',
  active: 'active',
};

export const keyEmitter = {
  exchangeGift: 'exchangeGift',
  updateProfile: 'updateProfile',
  updateOrderUser: 'updateOrderUser',
  changeTabBar: 'changeTabBar',
  tabBarName: 'tabBarName',
  deeplinkFacebook: 'deeplinkFacebookEvent',
};
