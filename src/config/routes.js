import React, {Component} from 'react';
import TakePhoto from '../containers/TakePhoto';
import {createStackNavigator} from 'react-navigation-stack';
import Authen from '../containers/Authen';
import RecordVideo from '../containers/RecordVideo';
import Result from '../containers/Result';
const AppStack = createStackNavigator(
  {
    TakePhoto: {screen: TakePhoto},
    RecordVideo: {screen: RecordVideo},
    Result: {screen: Result},
    Auth: Authen,
  },
  {
    initialRouteName: 'TakePhoto',
    headerMode: 'none',
    navigationOptions: {gesturesEnabled: true},
  },
);

const createRootNav = initRouteName => {
  return createStackNavigator(
    {
      App: AppStack,
      Auth: Authen,
    },
    {
      initialRouteName: initRouteName,
      headerMode: 'none',
    },
  );
};

export default createRootNav;
