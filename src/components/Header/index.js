import React, {Component} from 'react';
import {View, Text, TouchableOpacity, SafeAreaView} from 'react-native';

import {Button} from 'react-native-elements';
import {withNavigation} from 'react-navigation';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import Dimens from '../../config/Dimens';
FontAwesome.loadFont();

class Header extends Component {
  static defaultProps = {
    onPressBtn: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    var title = this.props.title;
    return (
      <View
        style={{
          width: '100%',
          height: 50,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          borderBottomWidth: 1,
          borderColor: '#DFE0E5',
        }}>
        <View style={{justifyContent: 'center', width: 40, marginLeft: 5}}>
          <TouchableOpacity
            onPress={() =>
              this.props.leftAction
                ? this.props.leftAction()
                : this.props.navigation.goBack()
            }>
            <FontAwesome
              name="chevron-left"
              size={17}
              color={'#000000'}
              style={{marginLeft: 10}}
            />
          </TouchableOpacity>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text
            numberOfLines={1}
            style={{
              color: '#000000',
              fontSize: Dimens.FontSize.Big,
              fontWeight: '600',
              marginHorizontal: 20,
            }}>
            {title}
          </Text>
        </View>
        <View style={{marginRight: 5, width: 40, justifyContent: 'center'}} />
      </View>
    );
  }
}

export default withNavigation(Header);
