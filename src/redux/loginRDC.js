// Actions.
export const SET_LOGIN = 'app/SET_LOGIN';
// Reducers.
const defaultState = {
  clientId: '',
};

export const loginReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_LOGIN:
      return {
        clientId: action.clientId,
      };
    default:
      return state;
  }
};

export function setLogin(clientId) {
  return {
    type: SET_LOGIN,
    clientId,
  };
}

export default loginReducer;
