// Actions.
export const GET_DATA_API = 'app/getDataAPI';
export const GET_DATA_API_SUCCESS = 'app/getDataAPISuccess';
export const GET_DATA_API_FAIL = 'app/getDataAPIFail';

// Reducers.
const defaultState = {
  data: null,
};

export const getDataAPIReducer = (state = defaultState, action) => {
  switch (action.type) {
    case GET_DATA_API:
      return state; //state.merge({});
    case GET_DATA_API_SUCCESS:
      // return state.merge(
      //   {
      //     data: action.data
      //   },
      //   { deep: true }
      // );
      return {data: action.data};
    case GET_DATA_API_FAIL:
      // return state.merge({
      //   data: null
      // });
      return {data: null};
    default:
      return state;
  }
};

// Action Creators.
// export function actGetDataAPI(action) {
//   return {
//     type: GET_DATA_API,
//     url: action.url,
//     name: action.name,
//     params: action.params
//   };
// }

export function actGetDataAPI(action) {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch({
        type: GET_DATA_API,
        url: action.url,
        notLoading: action.notLoading,
        notToast: action.notToast,
        name: action.name,
        params: action.params,
        token: action.token,
        method: action.method,
        clientId: action.clientId,
        resolve,
        reject,
      });
    });
  };
}

export default getDataAPIReducer;
