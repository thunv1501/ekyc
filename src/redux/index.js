import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import dataSaga from '../saga';
import {reducer as formReducer} from 'redux-form';
import getDataAPI from './getDataAPI';
import thunk from 'redux-thunk';
import tokenAppRDC from './tokenAppRDC';
import loginReducer from './loginRDC';

// import thunk from "redux-thunk";
/**
 * High Order Reducer object reducerName.
 */
export const reducerNameHOR = {
  getToken: 'getToken',
  factoryProduct: 'factoryProduct',
  getProvincial: 'getProvincial',
  getDistrict: 'getDistrict',
  getProductGroup: 'getProductGroup',
  searchProduct: 'searchProduct',
  getProductDetail: 'getProductDetail',
  getFactoryDetail: 'getFactoryDetail',
  getProductFactory: 'getProductFactory',
  getDiary: 'getDiary',
  getProductionArea: 'getProductionArea',
  getLand: 'getLand',
  getProductOfFactory: 'getProductOfFactory',
  getProductSession: 'getProductSession',
  getMaterialArea: 'getMaterialArea',
  createDiary: 'createDiary',
  login: 'login',
  logout: 'logout',
  deleteDiary: 'deleteDiary',
  searchFactory: 'searchFactory',
  getWard: 'getWard',
  getUserInfo: 'getUserInfo',
  updateProfile: 'updateProfile',
  getBanner: 'getBanner',
  getProcess: 'getProcess',
  getContactAppInfo: 'getContactAppInfo',
  productBySession: 'productBySession ',
  getStoreAppLink: 'getStoreAppLink',
  getProductByIndustry: 'getProductByIndustry',
  getNganh: 'getNganh',
  getNhom: 'getNhom',
  getPhanNhom: 'getPhanNhom',

  register: 'register',
  getHinhThaiTCSX: 'getHinhThaiTCSX',
  getDocument: 'getDocument',
  createDocument: 'createDocument',
  getDocumentByDocumentOCOP: 'getDocumentByDocumentOCOP',
  getProductOfUser: 'getProductOfUser',
  createProduct: 'createProduct',
  deleteProduct: 'deleteProduct',
  getDocumentType: 'getDocumentType',
  createDocumentInDocumentOCOP: 'createDocumentInDocumentOCOP',
  createDocumentOCOP: 'createDocumentOCOP',
  getCriteria: 'getCriteria',
  documentEdit: 'documentEdit',
  criteria: 'criteriaEdit',
  deleteDocument: 'deleteDocument',
  getCriteriaListDetail: 'getCriteriaListDetail',
  getCriteriaListDetailPoint: 'getCriteriaListDetailPoint',
  evaluationDocument: 'evaluationDocument',
  evaluationRecordsDetail: 'evaluationRecordsDetail',
  completeEvaluate: 'completeEvaluate',
  submitDocument: 'submitDocument',
  evaluationCriterial: 'evaluationCriterial',
  getPoint: 'getPoint',
};

/**
 * Create a function for High Order Reducer, reuse logic reducer.
 */
function createNamedWrapperReducer(reducerFunction, reducerName) {
  return (state, action) => {
    const {name} = action;
    const isInitializationCall = state === undefined;
    if (name !== reducerName && !isInitializationCall) {
      return state;
    }

    return reducerFunction(state, action);
  };
}

const appReducer = combineReducers({
  form: formReducer,
  tokenAppRDC,
  loginReducer,
  getToken: createNamedWrapperReducer(getDataAPI, reducerNameHOR.getToken),
  getDistrict: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getDistrict,
  ),
  getProvincial: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getProvincial,
  ),
  getProductType: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getProductType,
  ),
  searchProduct: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.searchProduct,
  ),
  getProductDetail: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getProductDetail,
  ),
  getFactoryDetail: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getFactoryDetail,
  ),
  getProductFactory: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getProductFactory,
  ),
  getDiary: createNamedWrapperReducer(getDataAPI, reducerNameHOR.getDiary),
  getProductionArea: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getProductionArea,
  ),
  getLand: createNamedWrapperReducer(getDataAPI, reducerNameHOR.getLand),
  getProductOfFactory: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getProductOfFactory,
  ),
  getProductSession: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getProductSession,
  ),
  getMaterialArea: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getMaterialArea,
  ),
  createDiary: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.createDiary,
  ),
  login: createNamedWrapperReducer(getDataAPI, reducerNameHOR.login),
  deleteDiary: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.deleteDiary,
  ),
  logout: createNamedWrapperReducer(getDataAPI, reducerNameHOR.logout),
  searchFactory: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.searchFactory,
  ),
  getWard: createNamedWrapperReducer(getDataAPI, reducerNameHOR.getWard),
  getUserInfo: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getUserInfo,
  ),
  updateProfile: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.updateProfile,
  ),
  getBanner: createNamedWrapperReducer(getDataAPI, reducerNameHOR.getBanner),
  getProcess: createNamedWrapperReducer(getDataAPI, reducerNameHOR.getProcess),
  getContactAppInfo: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getContactAppInfo,
  ),
  productBySession: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.productBySession,
  ),
  getStoreAppLink: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getStoreAppLink,
  ),
  getProductByIndustry: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getProductByIndustry,
  ),
  getNganh: createNamedWrapperReducer(getDataAPI, reducerNameHOR.getNganh),
  getNhom: createNamedWrapperReducer(getDataAPI, reducerNameHOR.getNhom),
  getPhanNhom: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getPhanNhom,
  ),
  register: createNamedWrapperReducer(getDataAPI, reducerNameHOR.register),

  getHinhThaiTCSX: createNamedWrapperReducer(getDataAPI, reducerNameHOR),
  getDocument: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getDocument,
  ),

  createDocument: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.createDocument,
  ),
  getDocumentByDocumentOCOP: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getDocumentByDocumentOCOP,
  ),
  getProductOfUser: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getProductOfUser,
  ),
  createProduct: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.createProduct,
  ),
  deleteProduct: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.deleteProduct,
  ),
  getDocumentType: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getDocumentType,
  ),
  createDocumentInDocumentOCOP: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.createDocumentInDocumentOCOP,
  ),
  getCriteria: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getCriteria,
  ),
  documentEdit: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.documentEdit,
  ),
  criteriaEdit: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.criteriaEdit,
  ),
  createDocumentOCOP: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.createDocumentOCOP,
  ),
  deleteDocument: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.deleteDocument,
  ),
  getCriteriaListDetail: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getCriteriaListDetail,
  ),
  evaluationDocument: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.evaluationDocument,
  ),
  evaluationRecordsDetail: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.evaluationRecordsDetail,
  ),
  completeEvaluate: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.completeEvaluate,
  ),
  submitDocument: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.submitDocument,
  ),
  evaluationCriterial: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.evaluationCriterial,
  ),
  getPoint: createNamedWrapperReducer(getDataAPI, reducerNameHOR.getPoint),
  getCriteriaListDetailPoint: createNamedWrapperReducer(
    getDataAPI,
    reducerNameHOR.getCriteriaListDetailPoint,
  ),
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

const sagaMiddleware = createSagaMiddleware();
const middlewares = [thunk, sagaMiddleware];

function configureStore() {
  const store = createStore(
    rootReducer,
    {},
    compose(applyMiddleware(...middlewares)),
  );
  sagaMiddleware.run(dataSaga);
  return store;
}

const store = configureStore();

export default store;
