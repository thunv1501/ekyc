// Actions.
export const TOKEN_APP = "app/TOKEN_APP";
export const REMOVE_TOKEN_APP = "app/REMOVE_TOKEN_APP"
// Reducers.
const defaultState = {
  token: {}
};

export const tokenReducer = (state = defaultState, action) => {
  switch (action.type) {
    case TOKEN_APP:
      return {
        token: action.token
      };
    case REMOVE_TOKEN_APP:
      return {
        token: {}
      };
    default:
      return state;
  }
};

export function saveToken(token) {
  return {
    type: TOKEN_APP,
    token
  };
}

export function removeToken() {
  return {
    type: REMOVE_TOKEN_APP,
  }
}

export default tokenReducer;
