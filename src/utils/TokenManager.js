import KJUR from "jsrsasign";
import * as Constants from "../config/constants";

export function validateToken(token) {
  try {
    return KJUR.jws.JWS.verifyJWT(token, Constants.PUBLIC_KEY_DECODE, {
      alg: ["RS256"],
      verifyAt: KJUR.jws.IntDate.get("now")
    });
  } catch (e) {
    return false;
  }
}

export function decodeToken(token) {
  try {
    return KJUR.jws.JWS.parse(token);
  } catch (e) {
    return false;
  }
}

export function encodeToken(header, payload) {
  try {
    return KJUR.jws.JWS.sign(
      null,
      header,
      payload,
      Constants.PRIVATE_KEY_ENDCODE
    );
  } catch (e) {
    return "";
  }
}
