import AsyncStorage from '@react-native-community/async-storage';
import Lodash from "lodash";

export const safeGetValueFromKey = (arrDataStorage, key) => {
  var valueStorage;
  if (Array.isArray(arrDataStorage) && arrDataStorage.length > 0) {
    for (var i = 0; i < arrDataStorage.length; i++) {
      if (
        Lodash.includes(arrDataStorage[i], key) &&
        Array.isArray(arrDataStorage[i]) &&
        arrDataStorage[i].length === 2
      ) {
        valueStorage = arrDataStorage[i][1];
      }
    }
  }

  return valueStorage;
};

export const getDataMultiKey = async arrKey => {
  var data = await AsyncStorage.multiGet(arrKey);
  var objectValue = {};
  if (Array.isArray(arrKey) && arrKey.length > 0) {
    for (var i = 0; i < arrKey.length; i++) {
      var valueStorage = safeGetValueFromKey(data, arrKey[i]);
      objectValue[arrKey[i]] = valueStorage;
    }
  }
  return objectValue;
};

export const saveDataFromKey = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {
    // Error saving data
    console.log("Error: ", error);
  }
};

export const getDataFromKey = async key => {
  var data;
  try {
    data = await AsyncStorage.getItem(key);
  } catch (error) {
    console.log("Error: ", error);
  }
  return data
};

export const removeDataFromKey = async key => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (error) {
    console.log("Error: ", error);
  }
};
