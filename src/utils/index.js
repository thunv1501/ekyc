import Lodash from "lodash";
import { Linking } from "react-native";
import { Platform, Dimensions, NetInfo } from "react-native";



import Moment from "moment";

const isIOS = Platform.OS === "ios";

export const isIphoneXorAbove = () => {
  const dimen = Dimensions.get("window");
  return (
    isIOS &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (dimen.height === 812 ||
      dimen.width === 812 ||
      (dimen.height === 896 || dimen.width === 896))
  );
};

export const formatData = (data, numColumns) => {
  if (data.length === 0) {
    return [];
  }
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
  while (
    numberOfElementsLastRow !== numColumns &&
    numberOfElementsLastRow !== 0
  ) {
    data.push({ id: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }

  return data;
};

export const gotoStoreDownloadApp = options => {
  if (isIOS) {
    var appStoreId = "1436926818";
    var urlHelper = `itms-apps://itunes.apple.com/us/app/id${appStoreId}?mt=8`;
    Linking.canOpenURL(urlHelper).then(supported => {
      if (supported) {
        Linking.openURL(urlHelper);
      } else {
        console.log("Don't know how to open URI: " + urlHelper);
      }
    });
  } else {
    var packageName = "com.ecomobi.teddybag";
    var urlHelper = `market://details?id=${packageName}`;
    Linking.canOpenURL(urlHelper).then(supported => {
      if (supported) {
        Linking.openURL(urlHelper);
      } else {
        console.log("Don't know how to open URI: " + urlHelper);
      }
    });
  }
};

export const getCurrentPosition = options => {
  return new Promise(function (resolve, reject) {
    navigator.geolocation.getCurrentPosition(resolve, reject, options);
  });
};

export const safeParseJson = jsonValue => {
  var objParsed = {};
  if (
    jsonValue &&
    jsonValue !== "" &&
    typeof jsonValue === "string" &&
    jsonValue !== null
  ) {
    objParsed = JSON.parse(jsonValue);
  }
  return objParsed;
};

export const isString = value => {
  var isStr = true;
  if (typeof value !== "string") {
    isStr = false;
  }

  return isStr;
};

export const replacePlusToSpace = strInput => {
  var replacedStr = strInput.split("+").join(" ");

  return replacedStr;
};

export const replaceSpaceToPlus = strInput => {
  var replacedStr = strInput.split(" ").join("+");

  return replacedStr;
};

export const getSafeValue = (object, keyItem, defaultValue) => {
  var safeValue = Lodash.get(object, keyItem, defaultValue);
  if (safeValue === null) {
    safeValue = defaultValue;
  }

  if (safeValue === "") {
    safeValue = defaultValue;
  }

  if (
    safeValue !== null &&
    defaultValue !== null &&
    (typeof safeValue !== typeof defaultValue ||
      safeValue.constructor !== defaultValue.constructor)
  ) {
    safeValue = defaultValue;
  }

  // console.log("safeValue", safeValue);

  return safeValue;
};


export const getParamURL = (urlInput, param) => {
  return decodeURI(
    urlInput.replace(
      new RegExp(
        "^(?:.*[&?]" +
        encodeURI(param).replace(/[.+*]/g, "$&") +
        "(?:=([^&]*))?)?.*$",
        "i"
      ),
      "$1"
    )
  );
};

const getConnectionInfo = async () => {
  if (isIOS) {
    return new Promise((resolve, reject) => {
      const connectionHandler = connectionInfo => {
        NetInfo.removeEventListener("connectionChange", connectionHandler);

        resolve(connectionInfo);
      };

      NetInfo.addEventListener("connectionChange", connectionHandler);
    });
  }

  return NetInfo.getConnectionInfo();
};

export const convertNumberWithCommas = num => {
  if (!Lodash.isNumber(num) && !Lodash.parseInt(num)) {
    return "0";
  }

  var strNumber = num.toString().replace(/ /g, "");

  return strNumber.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

export const convertNumberShort = num => {
  if (!Lodash.isNumber(num) && !Lodash.parseInt(num)) {
    return "0";
  }

  var strNumber = num.toString().replace(/ /g, "");

  if (strNumber.length < 3) {
    return "0";
  }

  var sliceNumber = strNumber.slice(0, -3);
  var numberShort = sliceNumber.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  return numberShort + "K";
};

const { width, height } = Dimensions.get("window");
export const isIphoneX =
  Platform.OS === "ios" &&
  !Platform.isPad &&
  !Platform.isTVOS &&
  (height === 812 || width === 812 || (height === 896 || width === 896));

export const isCloseToBottom = (
  { layoutMeasurement, contentOffset, contentSize },
  offsetList
) => {
  var paddingToBottom = 10;
  var isScrollDown = true;
  const currentOffset = contentOffset.y;
  const dif = currentOffset - (offsetList || 0);
  const isLongeContent = contentSize.height > layoutMeasurement.height;

  if (Math.abs(dif) < 3) {
    // console.log("unclear");
    isScrollDown = false;
  } else if (dif < 0) {
    // console.log("up");
    isScrollDown = false;
  } else {
    // console.log("down");
    isScrollDown = true;
  }

  offsetList = currentOffset;
  var isCloseToBot =
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
  return isCloseToBot && isScrollDown && isLongeContent;
};
